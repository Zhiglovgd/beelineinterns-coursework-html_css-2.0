//--------------------------1
const greetPerson = () => {
    const sayName = (name) => {
      return `hi, i am ${name}`
    }
    return sayName
  }
  
  const greeting = greetPerson(); // в greeting функция sayName(name).
  console.log(greeting('Pavel')); // sayName('Pavel') выведет "hi, i am Pavel".
  console.log(greeting('Irina')); // sayName('Irina') выведет "hi, i am Irina".
  
  //------------------------2
  let y = 'test';
  const foo = () => {
    var newItem = 'hello';
    console.log(y);
  }
  foo(); // выведется в консоле "test", поскольку y - глобакльная переменная.
  console.log(newItem); // локальная переменная функции foo, снаружи недоступна => ошибка.
  
  //------------------------3
  let y = 'test';
  let test = 2;
  const foo = () => {
    const test = 5;
    const bar = () => {
      console.log(5 + test);
    }
    bar()
  }
  foo();
// В консоли получим 10 так как первой переменной test найденной
// в Scope будет переменная во внешней функции (const test = 5;).  
  
  //-----------------------4
  const bar = () => {
    const b = 'no test'
  }
  bar();
  
  const foo = (() => {
    console.log(b);
  })(); // это IIFE объявляем функцию и сразу вызываем
  // в консоли получим ошибку поскольку к локальной 
  // переменной функции foo нет доступа, а Глобальная переменная объявлена после. 
  сonst b = 'test';