//-------1----------
const test = {} // пустой объект

if(test){  //Любой объект true в логическом контексте 
  console.log('i am here') // попадаем сюда
} else {
  console.log('now here')
}

//-------2----------
const date = new Date();
console.log(date < []) // дата преобразуется в целое количество миллисекунд, пустой массив = 0
// в консоле получим false

//-------3----------
const obj = {a: 2, b: 3};
console.log(obj == {a: 2, b: 3}) // сравниваются два разных объекта с одинаковым содержимым

//-------4----------
console.log(!{} + 'test') // !({} => true) => false + 'test' => 'falsetest'

//-------5----------
console.log({} + [1, 2]); // {} => "[object Object]"; [1, 2].toString => "1, 2" ; => "[object Object]1,2"