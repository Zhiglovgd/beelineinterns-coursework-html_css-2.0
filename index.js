let baseLevelTitles = document.querySelectorAll(
  ".block_base-level  .lesson-card__title"
);
for (item of baseLevelTitles) {
  item.textContent = item.textContent.toUpperCase();
}

let baseLevelDescriptions = document.querySelectorAll(
  ".block_base-level  .lesson-card__description"
);
for (item of baseLevelDescriptions) {
  if (item.textContent.trim().length > 20) {
    item.textContent = item.textContent.trim().slice(0, 20) + "...";
  }
}

//hwjs_7
const activeMenuItems = document.querySelectorAll(".menu-item__link-submenu");
for (item of activeMenuItems) {
  const submenu = item.nextElementSibling;
  item.addEventListener("click", (e) => {
    if (e.target.classList.contains("menu-item__link-submenu_active")) {
      submenu.style.height = 0;
    } else {
      submenu.style.height = submenu.children.length * 40 + "px";
    }
    e.target.classList.toggle("menu-item__link-submenu_active");
  });
}

//hwjs_8
const bodyAllLectionsBlock = document
  .querySelector(".block_all-lections")
  .querySelector(".lesson-list");
const allLections = document.querySelectorAll(".lesson-list__item");

window.onload = () => {
  let counterLabel = document.querySelector(".btn-primary_counter");

  counterLabel.textContent = allLections.length + " лекций";
  for (item of allLections) {
    bodyAllLectionsBlock.append(item.cloneNode(true));
  }
};

const filterButtons = document
  .querySelector(".block_all-lections")
  .querySelectorAll(".btn-primary_all");
for (item of filterButtons) {
  item.addEventListener("click", (e) => {
    let type;
    if (e.target.classList.contains("btn-primary_html")) {
      type = "html";
    }
    if (e.target.classList.contains("btn-primary_css")) {
      type = "css";
    }
    if (e.target.classList.contains("btn-primary_js")) {
      type = "js";
    }
    if (e.target.classList.contains("btn-primary_counter")) {
      type = "all";
    }
    filterByType(type);
  });
}

function filterByType(type) {
  if (type == "all") {
    bodyAllLectionsBlock.style.justifyContent = "space-between";
    bodyAllLectionsBlock.style.gap = "";
  } else {
    bodyAllLectionsBlock.style.justifyContent = "flex-start";
    bodyAllLectionsBlock.style.gap = "12px";
  }
  const lections = document
    .querySelector(".block_all-lections")
    .querySelectorAll(".lesson-list__item");
  for (item of lections) {
    if (type == "all") {
      item.style.display = "list-item";
    } else {
      if (item.hasAttribute("data-group")) {
        if (item.getAttribute("data-group") == type) {
          item.style.display = "list-item";
        } else {
          item.style.display = "none";
        }
      }
    }
  }
}

// hwjs_09
function getObjectFromLections() {
  const lectionsObj = {};
  for (let item of allLections) {
    if (item.hasAttribute('data-group')) {
      if (!(item.getAttribute('data-group') in lectionsObj)) {
        lectionsObj[item.getAttribute('data-group')] = [];
      }
      lectionsObj[item.getAttribute('data-group')].push(
        getObjectFromLection(item)
      );
    }
  }
  return lectionsObj;
}
function getObjectFromLection(item) {
  const lectionObj = {
    title: item.querySelector('.lesson-card__title')
      ? item.querySelector('.lesson-card__title').textContent
      : null,
    description: item.querySelector('.lesson-card__description')
      ? item.querySelector('.lesson-card__description').textContent.trim()
      : null,
    date: item.querySelector('.lesson-card__date')
      ? item.querySelector('.lesson-card__date').textContent
      : null,
    image: item.querySelector('.lesson-card__image').hasAttribute("src")
      ? item.querySelector('.lesson-card__image').getAttribute("src")
      : null,
    label: item.querySelector('.lesson-card__label')
      ? item.querySelector('.lesson-card__label').textContent
      : null,
  };
  return lectionObj;
}

console.log(getObjectFromLections());
